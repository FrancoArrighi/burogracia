using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float hours = 17;
    public float minutes = 0;

    public float FinishTime = 19;
    public float multiplier;

    bool timeRunning = true;

    [SerializeField] TMP_Text txt_hours;
    [SerializeField] TMP_Text txt_minutes;

    private void Start()
    {
        GameManager.instance.BossFinalActivated += StopTimer;
    }
    private void Update()
    {
        if (timeRunning)
        {
            if (hours < FinishTime)
            {
                minutes += Time.deltaTime * multiplier;
                txt_minutes.text = minutes.ToString("00");
                txt_hours.text = hours.ToString("00");
                if(minutes > 59)
                {
                    minutes = 0;
                    hours++;
                }
            }
            else
            {
                txt_hours.text = "0";
                timeRunning = false;
            }
        }
    }
    private void StopTimer()
    {
        timeRunning = false;
    }
}
