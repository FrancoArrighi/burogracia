using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class FireBullet : MonoBehaviour
{
    public int bulletAmount = 40;
    public float spreadAngle = 360f; // Total angle of bullet spread

    public GameObject bulletPrefab;

    public Vector3 bulletDirection;

    public float fireRate = 0.5f;

    public int prevFibonacciNum = 0;
    private int currentFibonacciNum = 1;

    private float angle = 0f;

    private void Start()
    {

        InvokeRepeating("FireBullets", 5f, 2f);
    }
    public void FireBullets()
    {
        Fire(SetRandomNum());
    }
    public int SetRandomNum()
    {
        return Random.Range(0, 3);
    }
    public void Fire(int fireType)
    {
        CancelInvoke("FibonacciPattern");
        CancelInvoke("DefaultPattern");
        CancelInvoke("CircularPattern");

        angle = 0f;
        ResetFibonacciSequence();

        switch (fireType)
        {
            case 0:
                {
                    InvokeRepeating("DefaultPattern", 0, 0.5f);
                }
                break;
            case 1:
                {
                    InvokeRepeating("FibonacciPattern", 0, 0.5f);
                }
                break;
            case 2:
                {
                    InvokeRepeating("CircularPattern", 0, 0.5f);
                }
                break;
            default:
                break;
        }
    }

    public void DefaultPattern()
    {
        float angleStep = spreadAngle / bulletAmount;

        for (int i = 0; i < bulletAmount; i++)
        {
            float bulletDirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float bulletDirY = transform.position.y;
            float bulletDirZ = transform.position.z + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 bulletMoveVector = new Vector3(bulletDirX, bulletDirY, bulletDirZ);
            Vector3 bulletDir = (bulletMoveVector - transform.position).normalized;

            GameObject bullet = InstantiateBullet(bulletDir);
            Bullet bulletComp = bullet.GetComponent<Bullet>();
            bulletComp.SetBullet(12f, 0f, 1f, Vector3.forward);


            angle += angleStep;
        }
    }
  
    public void CircularPattern()
    {
        float angleStep = spreadAngle / bulletAmount;

        for (int i = 0; i < bulletAmount; i++)
        {
            float bulletDirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float bulletDirY = transform.position.y;
            float bulletDirZ = transform.position.z + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 bulletMoveVector = new Vector3(bulletDirX, bulletDirY, bulletDirZ);
            Vector3 bulletDir = (bulletMoveVector - transform.position).normalized;

            GameObject bullet = InstantiateBullet(bulletDir);
            Bullet bulletComp = bullet.GetComponent<Bullet>();
            bulletComp.SetBullet(13f, 0.5f, 1f, Vector3.forward);


            angle += angleStep;
        }
    }
    void FibonacciPattern()
    {
        float fibonacciDistance = GetNextFibonacciDistance();

        for (int i = 0; i < bulletAmount; i++)
        {
            float bulletDirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f) * fibonacciDistance;
            float bulletDirY = transform.position.y;
            float bulletDirZ = transform.position.z + Mathf.Cos((angle * Mathf.PI) / 180f) * fibonacciDistance;

            Vector3 bulletMoveVector = new Vector3(bulletDirX, bulletDirY, bulletDirZ);
            Vector3 bulletDir = (bulletMoveVector - transform.position).normalized;

            GameObject bullet = InstantiateBullet(bulletDir);
            Bullet bulletComp = bullet.GetComponent<Bullet>();
            bulletComp.SetBullet(15f, 0.3f, 1f, bulletDir);

            angle += 360f / bulletAmount;  // Dividir 360 grados entre el n�mero de balas para obtener el siguiente �ngulo
        }

        if (angle >= 360f)
        {
            angle -= 360f;
        }
    }

    private float GetNextFibonacciDistance()
    {
        int nextNum = prevFibonacciNum + currentFibonacciNum;
        prevFibonacciNum = currentFibonacciNum;
        currentFibonacciNum = nextNum;
        return nextNum;
    }

    private GameObject InstantiateBullet(Vector3 bulletDir)
    {
        GameObject bullet = Instantiate(bulletPrefab);
        bullet.transform.position = transform.position;
        bullet.transform.rotation = Quaternion.LookRotation(bulletDir);

        bullet.SetActive(true);
        return bullet;
    }

    private void ResetFibonacciSequence()
    {
        prevFibonacciNum = 0;
        currentFibonacciNum = 1;
    }
}
