using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerBoss : MonoBehaviour
{
    public GameObject winCondition;
    public GameObject msj_Loss;
    public GameObject msj_Win;

    public Animator fade_animator;

    public static GameManagerBoss Instance;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        Invoke("HideText", 5f);
    }

    public void HideText()
    {
        winCondition.SetActive(false);
    }
    public void ResetLvl()
    {
        msj_Loss.SetActive(true);
        fade_animator.Play("Fade");
        Invoke("ResetLevel", 4f);
    }
    public void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void TerminarJuego()
    {
        Time.timeScale = 0f;
        msj_Win.SetActive(true);
    }
}
