using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBossFinal : MonoBehaviour
{
    public float velocidad = 0;

    public int lives = 0;

    public TMP_Text txt_lives;
    private void FixedUpdate()
    {
        txt_lives.text = lives.ToString();
        Mover();
    }
    private void Mover()
    {
        float movHorizontal = Input.GetAxisRaw("Horizontal") * velocidad;
        float movVertical = Input.GetAxisRaw("Vertical") * velocidad;

        Vector3 movementDirection = new Vector3(movHorizontal, 0, movVertical);

        transform.Translate(movementDirection * velocidad * Time.deltaTime, Space.World);
    }
    public void TakeDamage()
    {
        lives--;
        if (lives <= 0) { lives = 0; Die(); }
        txt_lives.text = lives.ToString();

    }

    private void Die()
    {
        GameManagerBoss.Instance.ResetLvl();
    }
}
