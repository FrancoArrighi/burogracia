using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletLife = 1f;

    public float speed = 15;

    public Vector3 rotation;
    public Vector3 moveDirection;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.GetComponent<PlayerBossFinal>().TakeDamage();
            DestroyBullet();
        }
        if (other.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        Invoke("DestroyBullet", bulletLife);
    }

    private void Update()
    {
        transform.Translate(moveDirection * speed * Time.deltaTime);
    }
    private void FixedUpdate()
    {
        transform.Rotate(rotation);
    }
    private void SetRotation(float _rotation)
    {
        rotation = new Vector3(0f, _rotation, 0f);
    }
    private void SetScale(float scale)
    {
        transform.localScale = new Vector3(scale, scale, scale);
    }
    private void SetSpeed(float _speed)
    {
        speed = _speed;
    }
    public void SetMoveDir(Vector3 dir)
    {
        moveDirection = dir;
    }
    public void SetBullet(float _speed, float _rotation, float _scale, Vector3 dir)
    {
        SetRotation(_rotation);
        SetScale(_scale);
        SetSpeed(_speed);
        SetMoveDir(dir);
    }
    public void DestroyBullet()
    {
        Destroy(this.gameObject);
    }
}
