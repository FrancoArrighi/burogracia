using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossMov : MonoBehaviour
{
    private NavMeshAgent agent;
    public float range = 15.0f;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        MoveToRandomPosition();
    }

    public void DestroyStudent()
    {
        Destroy(this.gameObject);
    }
    void MoveToRandomPosition()
    {
        while (true)
        {
            Vector3 randomDirection = Random.insideUnitSphere * range;
            randomDirection += transform.position;
            NavMeshHit hit;

            if (NavMesh.SamplePosition(randomDirection, out hit, range, NavMesh.GetAreaFromName("Boss")))
            {
                if (agent.pathStatus == NavMeshPathStatus.PathComplete)
                {
                    agent.SetDestination(hit.position);
                    Debug.DrawRay(hit.position, Vector3.up * 0.5f, Color.blue, 1.0f);
                    break;
                }
            }
        }
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, agent.destination) <= 2.0f)
        {
            MoveToRandomPosition();
        }
    }
}
