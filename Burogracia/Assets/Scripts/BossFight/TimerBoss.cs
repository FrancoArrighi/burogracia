using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimerBoss : MonoBehaviour
{
    public float StartTime = 900f;
    public float time = 900f;
    bool timeRunning = true;
    [SerializeField] TMP_Text timer;

    void Start()
    {
        time = StartTime;

    }

    private void Update()
    {
        if (timeRunning)
        {
            if (time > 0)
            {
                time -= Time.deltaTime;


                int minutos = Mathf.FloorToInt(time / 60f);
                int segundos = Mathf.FloorToInt(time % 60f);
                timer.text = minutos.ToString("00") + ":" + segundos.ToString("00");

            }
            else
            {
                timer.text = "0";              
                timeRunning = false;
                GameManagerBoss.Instance.TerminarJuego();
            }
        }
    }
}
