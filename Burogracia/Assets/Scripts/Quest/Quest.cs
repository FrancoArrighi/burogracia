using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Quest 
{
    public string title;
    [TextArea(5,10)]
    public string description;

    public bool isActive;
    public bool isCompleted;
    public bool objRequired;
    public bool objObtained;
    public Sprite sprite;
}
