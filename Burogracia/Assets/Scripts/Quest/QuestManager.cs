using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{
    public GameObject QuestWindow;

    public Quest quest;
    public TMP_Text txt_title;
    public TMP_Text txt_description;
    public Image image;

    public static QuestManager instance;
    private void Awake()
    {
        instance = this;
    }

    public void ShowQuest()
    {
        txt_title.text = quest.title;
        txt_description.text = quest.description;
        image.sprite = quest.sprite;
        quest.isActive = true;
        GameManager.activeQuest = quest;
        if (quest.title != "")
            QuestWindow.SetActive(true);
    }
    public void HideQuest()
    {
        quest.isCompleted = true;
        GameManager.activeQuest = null;
        QuestWindow.SetActive(false);
    }
}
