using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;

    public bool triggered = false;

    public Quest quest;
    public GameObject iconMissionActive;

    public void ShowDialogue()
    {
        QuestManager.instance.HideQuest();
        if (GameManager.activeQuest != null) // si no es la primera 
        {
            GameManager.activeQuest.isCompleted = true;
            GameManager.instance.questCompleted++;

        }
        GameManager.activeQuest = quest;
        triggered = true;
        iconMissionActive.SetActive(false);
        DialogueManager.Instance.StartDialogue(dialogue);
    }

}