using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Player player;
    public CameraMov cam;
    Queue<string> sentences = new Queue<string>();

    public TMP_Text txt_name;
    public TMP_Text txt_dialogue;
    public Image imageCharacter;

    public GameObject dialogueContainer;

    public List<DialogueTrigger> triggers = new List<DialogueTrigger>();
    public int countTrigger = 0;

    public static DialogueManager Instance;
    private void Awake()
    {
        Instance = this;
        cam = Camera.main.GetComponent<CameraMov>();

    }

    public void StartDialogue(Dialogue dialogue)
    {
        cam.SetDialogueCursor();
        txt_name.text = dialogue.name;
        imageCharacter.sprite = dialogue.imageCharacter;
        sentences.Clear();
        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        dialogueContainer.SetActive(true);
        ShowNextSentence();
    }
    public void ShowNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        string sentence = sentences.Dequeue();
        txt_dialogue.text = sentence;
    }
    public void EndDialogue()
    {
        QuestManager.instance.ShowQuest();
        cam.SetFirstPersonCursor();
        dialogueContainer.SetActive(false);
        player.ActivatePlayer();
        countTrigger++;
        ActivateDialogueTrigger();
        if (countTrigger == 3)
        {
            PrinterManager.instance.ActivateObjects();
        }
        if (countTrigger == 6)
        {
            SceneManager.LoadScene(1);
        }
    }

    public void ActivateDialogueTrigger() // llamarlo cuando tengo la fotocopa 
    {
        if (triggers.Count > countTrigger && (!GameManager.activeQuest.objRequired || GameManager.activeQuest.objObtained))
        {
            triggers[countTrigger].gameObject.SetActive(true);
        }
    }
}
