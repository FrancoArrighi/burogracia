using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    CameraMov camMov;
    public float vel;
    Rigidbody rb;
    public GameObject interactionImg;

    public bool nearInteraction = false;

    public DialogueTrigger ActualDialogueTrigger;
    private void Start()
    {
        camMov = Camera.main.GetComponent<CameraMov>();
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        HandleMovement();

        if (nearInteraction && Input.GetMouseButton(0))
        {
            ActualDialogueTrigger.ShowDialogue();
            interactionImg.SetActive(false);
            QuestManager.instance.quest = ActualDialogueTrigger.quest;    
            DesactivatePlayer();
        }
    }
    public void HandleMovement()
    {
        float movVertical = Input.GetAxisRaw("Vertical");
        float movHorizontal = Input.GetAxisRaw("Horizontal");

        Vector3 velocity = Vector3.zero;
        if (movVertical != 0 || movHorizontal != 0)
        {
            Vector3 dir = (transform.forward * movVertical + transform.right * movHorizontal).normalized;
            velocity = dir * vel;
        }
        rb.velocity = velocity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Interactable"))
        {
            if (!other.GetComponent<DialogueTrigger>().triggered)
            {
                ActualDialogueTrigger = other.GetComponent<DialogueTrigger>();
                nearInteraction = true;
                interactionImg.SetActive(true);
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.CompareTag("Interactable"))
        {
            interactionImg.SetActive(false);
            nearInteraction = false;
        }
    }
    public void DesactivatePlayer()
    {
        camMov.enabled = false;
        this.enabled = false;
    }
    public void ActivatePlayer()
    {
        camMov.enabled = true;
        this.enabled = true;
        nearInteraction = false;
        ActualDialogueTrigger = null;
    }
}
