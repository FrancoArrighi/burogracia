using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrinterManager : MonoBehaviour
{
    public List<GameObject> objectsInOrder;
    public int currentIndex = 0;

    public static PrinterManager instance;
    private void Start()
    {
        instance = this;
        currentIndex = 0;
    }

    public void ObjectTouched(GameObject touchedObject)
    {
        if (touchedObject == objectsInOrder[currentIndex])
        {
            touchedObject.GetComponent<PrinterStep>().ChangeMat(1);
            currentIndex++;

            if (currentIndex >= objectsInOrder.Count)
            {
                CompleteTask();
            }
        }
        else
        {
            ResetSequence();
        }
    }

    public void ActivateObjects()
    {
        foreach (GameObject obj in objectsInOrder) { obj.GetComponent<PrinterStep>().enabled = true; }
    }
    public void DesactivateObjects()
    {
        foreach (GameObject obj in objectsInOrder) { obj.GetComponent<PrinterStep>().enabled = false; }
    }
    private void ResetSequence()
    {
        currentIndex = 0;
        foreach (GameObject obj in objectsInOrder) { obj.GetComponent<PrinterStep>().ResetComponent(); }
        // Aqu� podr�as agregar cualquier l�gica adicional, como restablecer el estado de los objetos, etc.
    }

    private void CompleteTask()
    {
        GameManager.activeQuest.objObtained = true;
        DesactivateObjects();
        DialogueManager.Instance.ActivateDialogueTrigger();
        Debug.Log("Tarea completada!");
    }
}
