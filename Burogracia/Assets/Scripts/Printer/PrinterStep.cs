using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrinterStep : MonoBehaviour
{
    public bool nearplayer = false;
    public Material[] mats;
    public MeshRenderer meshrenderer;

    private void Start()
    {
        meshrenderer = GetComponent<MeshRenderer>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && this.enabled)
        {
            ChangeMat(1);
            nearplayer = true;

        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && this.enabled)
        {
            ChangeMat(0);
            nearplayer = false;

        }
    }
    public void ChangeMat(int index)
    {
        meshrenderer.material = mats[index];
    }
    public void ResetComponent()
    {
        ChangeMat(0);
    }
    private void Update()
    {
        if (nearplayer && Input.GetMouseButtonDown(0))
        {
            PrinterManager.instance.ObjectTouched(this.gameObject);
        }
    }
}
